package com.akordirect.udemy3;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.GridLayout;
import android.widget.ImageView;
import android.widget.TextView;


public class GameTickTackActivity extends AppCompatActivity {

    private int[] gameCells = {2, 2, 2, 2, 2, 2, 2, 2, 2};
    private int[][] winningPositions = {{0, 1, 2}, {3, 4, 5}, {6, 7, 8}, {0, 3, 6}, {1, 4, 7}, {2, 5, 8}, {0, 4, 8}, {2, 4, 6}};
    private int activePlayer = 0;
    private boolean gameActive = true;

    public void dropIn(View view) {
        ImageView imageView = (ImageView) view;

        int tappedCell = Integer.parseInt(imageView.getTag().toString());
        Log.d("Tag", "tapped cell: " + tappedCell);

        if (gameCells[tappedCell] == 2 && gameActive) {

            gameCells[tappedCell] = activePlayer;
            imageView.setTranslationY(-1500);

            if (activePlayer == 0) {
                imageView.setImageResource(R.drawable.circle);
                activePlayer = 1;
            } else {
                imageView.setImageResource(R.drawable.cross);
                activePlayer = 0;
            }


        imageView.animate().translationYBy(1500).rotation(3520).setDuration(300);

        for (int[] winningPosition : winningPositions) {

            if (gameCells[winningPosition[0]] == gameCells[winningPosition[1]] &&
                gameCells[winningPosition[1]] == gameCells[winningPosition[2]] &&
                gameCells[winningPosition[0]] != 2) {

                String winner =  activePlayer == 1 ? "O" : "X";
//                Toast.makeText(this, winner + " has won", Toast.LENGTH_LONG).show();

                TextView textView = findViewById(R.id.textView_win);
                Button button = findViewById(R.id.button_win);

                textView.setText(winner + " has won");
                textView.setVisibility(View.VISIBLE);
                button.setVisibility(View.VISIBLE);

                gameActive = false;
            }
        }
    } // finish first IF
    }


    public void startNewGame(View view){

        Button button = findViewById(R.id.button_win);
        TextView textView = findViewById(R.id.textView_win);

        button.setVisibility(View.INVISIBLE);
        textView.setVisibility(View.INVISIBLE);

        GridLayout gridLayout = (GridLayout) findViewById(R.id.layout_grid);
        for(int i=0;i<gridLayout.getChildCount(); i++){
            ImageView counter = (ImageView) gridLayout.getChildAt(i);
            counter.setImageDrawable(null);
        }
//           gameCells = {2, 2, 2, 2, 2, 2, 2, 2, 2};
        for (int i = 0; i < gameCells.length; i++ ) {
            gameCells[i] = 2;
            Log.d("Log dm", "gameCells" + i +" = " + gameCells[i] );
        }

           activePlayer = 0;
           gameActive = true;
           Log.d("Log..", "" + gameActive );
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game_tick_tack);
    }


}
