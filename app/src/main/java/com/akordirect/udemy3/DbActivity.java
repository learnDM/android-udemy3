package com.akordirect.udemy3;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class DbActivity extends AppCompatActivity {

    private SQLiteDatabase database;
    private ListView listView_db;
    private TextView textView_db;
    List<String> feelings = new ArrayList<>();

    private static final String TAG = "DbActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_db);

        textView_db = findViewById(R.id.textView_db);
        listView_db = findViewById(R.id.listView_db);



        try {
            database = this.openOrCreateDatabase("dm_test", MODE_PRIVATE, null);
            String sql = "create table if not exists users (name varchar, age int(3))";
//            String sql = "drop table newusers";
//            Log.d(TAG, "request in DB: " + sql);
//            database.execSQL(sql);

//            String sql = "drop table users";
//            Log.d(TAG, "request in DB: " + sql);
//            database.execSQL(sql);

//            String sql = "create table if not exists newusers (_id INTEGER(3) primarykey, name varchar, age int(3))";
            Log.d(TAG, "request in DB: " + sql);
            database.execSQL(sql);
//    sql = "insert into users (name, age) values ('Dmitry', 38)";
//    database.execSQL(sql);


//    sql = "delete from users where name ='Dave')"; // delete
//    database.execSQL(sql);


            Cursor c = database.rawQuery("select * from users", null);
            int nameIndex = c.getColumnIndex("name");
            int ageIndex = c.getColumnIndex("age");

            if (c != null) {
                c.moveToFirst();

                do {
                    String nameFromDB = c.getString(nameIndex);
                    Log.i(TAG, "nameIndex = " + nameFromDB + " - " + c.getString(ageIndex));
                    feelings.add(nameFromDB);
                } while (c.moveToNext());


            }

        } catch (Exception e) {
            Log.e(TAG, "Error with Db: " + e.getMessage());
        }


        ArrayAdapter<String> adapter =
                new ArrayAdapter<>(DbActivity.this, android.R.layout.simple_list_item_1, feelings);
        listView_db.setAdapter(adapter);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
// Inflate the menu; this adds items to the action bar if it is present.
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.menu_db, menu);


        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        super.onOptionsItemSelected(item);

        switch (item.getItemId()) {
            case R.id.action_db_settings1:
                Log.i(TAG, "Clicked settings 1 ");
                return true;
            case R.id.action_db_settings2:
                Log.i(TAG, "Clicked settings  2 ");
                return true;
            default:
                return false;
        }
    }


}
