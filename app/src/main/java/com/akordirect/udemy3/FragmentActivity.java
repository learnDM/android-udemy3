package com.akordirect.udemy3;

import androidx.appcompat.app.AppCompatActivity;


import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.view.View;

import com.akordirect.udemy3.fragments.FragmentFirst;
import com.akordirect.udemy3.fragments.FragmentSecond;

public class FragmentActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fragment);
    }

    public void selectFragment(View view) {
        Fragment fragment;
        if (view == findViewById(R.id.buttonFirst)) {
            fragment = new FragmentFirst();
        } else {
            fragment = new FragmentSecond();
        }

        FragmentManager fm = getFragmentManager();
        FragmentTransaction fragmentTransaction = fm.beginTransaction();
        fragmentTransaction.replace(R.id.fragment, fragment);
        fragmentTransaction.commit();

    }
}
