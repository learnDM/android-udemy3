package com.akordirect.udemy3;

import androidx.appcompat.app.AppCompatActivity;

import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.MediaController;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.VideoView;

public class VideoViewActivity extends AppCompatActivity {

    private TextView textView;
    // Video
    private VideoView videoView;
    private MediaPlayer mediaPlayer;
    //Audio
    private SeekBar volumeControl;
    private AudioManager audioManager;


    public void playAudio(View view){
        mediaPlayer.start();
    }
    public void pauseAudio(View view){
        mediaPlayer.pause();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_view);

        // Initializaton
        videoView = findViewById(R.id.videoView_dm);
        mediaPlayer = MediaPlayer.create(this,   R.raw.audio_possessives);
        volumeControl = findViewById(R.id.seekBar_dm);
        textView = findViewById(R.id.textView_audio);

        audioManager = (AudioManager) getSystemService(AUDIO_SERVICE);

        // Video
        videoView.setVideoPath("android.resource://"+getPackageName() + "/" + R.raw.demovideo);
        MediaController mediaController = new MediaController(this);
        mediaController.setAnchorView(videoView);
        videoView.setMediaController(mediaController);
        videoView.start();

        // Audio
        int maxVolume = audioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
        int currentVolume = audioManager.getStreamVolume(AudioManager.STREAM_MUSIC);

        volumeControl.setMax(maxVolume);
        volumeControl.setProgress(currentVolume);

        volumeControl.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                Log.d("SeekBar Change dm : ", "" + i );
                textView.setText("Audio -> " + i);
                audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, i, 0);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                textView.setText("Audio");
            }
        });

    }
}
