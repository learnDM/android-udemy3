package com.akordirect.udemy3;

import androidx.appcompat.app.AppCompatActivity;

import android.media.MediaPlayer;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class BasicPhrasesActivity extends AppCompatActivity {

    private static final String TAG = "BasicPhrasesActivity";
    private MediaPlayer mPlayer;

    List<MediaPlayer> players = new ArrayList<>();

    // Phrases have recived from https://ielanguages.com/
    private boolean fileNotPlaying = true;

    public void clickButton (View view){

        Button pressedButton = (Button) view;
        String nameOfFile = pressedButton.getTag().toString();

        Log.d(TAG, "Audio is playing: " + nameOfFile);

        mPlayer = MediaPlayer.create(this, getResources().getIdentifier(nameOfFile, "raw", getPackageName()));
        players.add(mPlayer);
        mPlayer.start();
//       if(fileNotPlaying){
//           mPlayer.start();
//           fileNotPlaying = false;
//           Log.d(TAG, "fileNotPlaying = false." );
//       }
//       else {
//           mPlayer.pause();
//           fileNotPlaying=true;
//           Log.d(TAG, "fileNotPlaying = true." );
//       }
    }

//    public void stopPlayingAll(View view){
//
//        for (MediaPlayer mp : players){
//            if(mp!=null && mp.isPlaying()){
//                mp.reset();
//                try {
//                    mp.prepare(); // Does not work
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//                mp.stop();
//                mp.release();
//                players.remove(mp);
//            }
//        }
//    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_basic_phrases);
    }
}
