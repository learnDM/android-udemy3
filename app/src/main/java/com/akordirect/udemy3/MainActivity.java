package com.akordirect.udemy3;

import android.app.Notification;
import android.app.PendingIntent;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import androidx.core.app.TaskStackBuilder;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.navigation.NavigationView;
import com.google.android.material.snackbar.Snackbar;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private static final String TAG = "MainActivity";
    private CountDownTimer countDownTimer;
    private TextView textView;
    private TextView textViewYourFeelings;
    private Button buttonSaveInDb;
    private EditText editTextFeelings;
    private SQLiteDatabase database;

    private String active = "0";





    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, "OnCreate");
        setContentView(R.layout.activity_main);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);


//        dm
        textView = findViewById(R.id.textView_main_dm);
        textViewYourFeelings = findViewById(R.id.textView_how_your_feelings);
        buttonSaveInDb = findViewById(R.id.button_save_in_DB);
        editTextFeelings = findViewById(R.id.editText_feelings);

        buttonSaveInDb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                saveFeelingsInDb();
            }
        });

        database = this.openOrCreateDatabase("dm_test", MODE_PRIVATE, null);
        String sql = "create table if not exists users (name varchar, age int(3))";
        database.execSQL(sql);


        if(savedInstanceState!=null) {
            active = savedInstanceState.getString("tt");
            Log.d(TAG, "savedInstanceState!=null and active = " + active);
        }else {
            Log.d(TAG, "savedInstanceState==null");

        }

        // dmon
//int tt = 3600000; // equals 1 hour
        Log.d(TAG,"active.equals  " + active );
        if(active.equals("0")) {
            Log.i(TAG, "CountDownTimer is created!");
            countDownTimer = new CountDownTimer(600000, 1000) {
                @Override
                public void onTick(long l) {
                    textView.setText("Timer is " + l / 1000);
                    Log.d(TAG, "Timer is " + l / 1000);
                }

                @Override
                public void onFinish() {
                    doWhenTimeIsFinished();
                }

            };
            countDownTimer.start();
        }
    }



    public void saveFeelingsInDb( ){
        Log.d(TAG,"Data is saved in Data Base!");
        String stringCondition = editTextFeelings.getText().toString();
        String sql = "insert into users (name, age) values ('"+stringCondition+"', 1)";
        database.execSQL(sql);

        buttonSaveInDb.setVisibility(View.INVISIBLE);
        editTextFeelings.setVisibility(View.INVISIBLE);
        textViewYourFeelings.setText("Ok, Thanks! Inf is saved in DB.");
    }



   private void doWhenTimeIsFinished(){
        textView.setText("Time is finished!" );
        Log.d(TAG,"Ok. All is good!" );

        Toast.makeText(MainActivity.this, "Game: Timer is finished", Toast.LENGTH_SHORT).show();

        // Create an Intent for the activity you want to start
        Intent resultIntent = new Intent(MainActivity.this, MainActivity.class);
        // Create the TaskStackBuilder and add the intent, which inflates the back stack
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(MainActivity.this);
        stackBuilder.addNextIntentWithParentStack(resultIntent);
        // Get the PendingIntent containing the entire back stack
        PendingIntent resultPendingIntent =
                stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);

        Notification notification = new NotificationCompat.Builder(MainActivity.this, "channel01")
                .setSmallIcon(android.R.drawable.ic_dialog_info)
                .setContentTitle("Test")
                .setContentText("You see me!")
                .setDefaults(Notification.DEFAULT_ALL)
                .setPriority(NotificationCompat.PRIORITY_HIGH)   // heads-u
                .setContentIntent(resultPendingIntent)
                .setAutoCancel(true)
                .build();

        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(MainActivity.this);
        notificationManager.notify(0, notification);
    }


    @Override
    protected void onRestoreInstanceState(@NonNull Bundle savedInstanceState) {
        Log.d(TAG, "onRestoreInstanceState");
        active = savedInstanceState.getString("tt");
        Log.d(TAG, "onRestoreInstanceState tt = " + active);
        super.onRestoreInstanceState(savedInstanceState);
    }

    @Override
    protected void onStart() {
        Log.d(TAG, "OnStart");
        super.onStart();
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(TAG, "OnResume");
    }

    @Override
    protected void onPause() {
        Log.d(TAG, "OnPause");
        super.onPause();
    }

    @Override
    protected void onRestart() {
        Log.d(TAG, "onRestart");
        super.onRestart();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putString("tt", "1");
        super.onSaveInstanceState(outState);
        Log.d(TAG, "onSaveInstanceState (DM)");
    }

    @Override
    protected void onStop() {
        Log.d(TAG, "onStop");
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        Log.d(TAG, "OnDestroy (MainActivity)");
        super.onDestroy();
    }



    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_home) {
            // Handle the camera action
            Intent intentFadingAnimation = new Intent(this, AnimationFadeActivity.class);
            startActivity(intentFadingAnimation);
        }
//        else if (id == R.id.nav_gallery) {
//            Intent intentFadingAnimation = new Intent(this, AnimationOtherActivity.class);
//            startActivity(intentFadingAnimation);
//        }
        else if (id == R.id.nav_slideshow) {
            Intent intentFadingAnimation = new Intent(this, GameTickTackActivity.class);
            startActivity(intentFadingAnimation);
        }
//        else if (id == R.id.nav_tools) {
//
//        } else if (id == R.id.nav_share) {
//
//        } else if (id == R.id.nav_send) {
//
//        }
          else if (id == R.id.nav_video) {
            Intent intentVideo = new Intent(this, VideoViewActivity.class);
            startActivity(intentVideo);
        } else if (id == R.id.nav_phrases_up) {
            Intent intentVideo = new Intent(this, BasicPhrasesActivity.class);
            startActivity(intentVideo);
        } else if (id == R.id.nav_view_list) {
            Intent intentViewList = new Intent(this, ListViewActivity.class);
            startActivity(intentViewList);
        }else if (id == R.id.nav_db) {
            Intent intentViewList = new Intent(this, DbActivity.class);
            startActivity(intentViewList);
        }else if (id == R.id.nav_google_map) {
            Intent intentViewList = new Intent(this, GoogleMapActivity.class);
            startActivity(intentViewList);
        }else if (id == R.id.nav_alarm) {
            Intent intentViewList = new Intent(this, AlarmActivity.class);
            startActivity(intentViewList);
        }else if (id == R.id.nav_egg_timer) {
            Intent intentViewList = new Intent(this, EggActivity.class);
            startActivity(intentViewList);
        }else if (id == R.id.nav_download) {
            Intent intent = new Intent(this, L76DownloadActivity.class);
            startActivity(intent);
        }else if (id == R.id.nav_94l_location) {
            Intent intent = new Intent(this, L94LocationActivity.class);
            startActivity(intent);
        }else if (id == R.id.nav_fragments) {
            Intent intent = new Intent(this, FragmentActivity.class);
            startActivity(intent);
        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }





}
