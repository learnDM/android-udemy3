package com.akordirect.udemy3;

import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;

public class AnimationFadeActivity extends AppCompatActivity {
    private static final String TAG = "AnimationFadeActivity";
    private ImageView imageCat;
    private ImageView imageCat2;
    private boolean cat1IsShowing = true;
    private TypeOfEvent typeOfEvent = TypeOfEvent.FADING;


    private enum TypeOfEvent {
        ROTATE_CW, ROTATE_CCW,  FADING
    }

    public void clickCat(View view) {

        if (typeOfEvent == TypeOfEvent.FADING) {

            if (cat1IsShowing) {
                cat1IsShowing = false;
                imageCat.animate().alpha(0).setDuration(2000);
                imageCat2.animate().alpha(1).setDuration(2000);
            } else {
                cat1IsShowing = true;
                imageCat.animate().alpha(1).setDuration(2000);
                imageCat2.animate().alpha(0).setDuration(2000);
            }
        } else if (typeOfEvent == TypeOfEvent.ROTATE_CW) {
            ImageView view1 = (ImageView) view;
            view1.animate().rotation(360).alpha(1).setDuration(1000);
//            view1.animate().rotation(-360).setDuration(1000);
            Log.i(TAG, "-- Rotating 360 ---");
        }else if (typeOfEvent == TypeOfEvent.ROTATE_CCW) {
            ImageView view1 = (ImageView) view;
            view1.animate().rotation(-360).setDuration(1000);
            Log.i(TAG, "-- Rotating -360 ---");
        }

    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_animation_fade);
//
        imageCat = findViewById(R.id.imageView_cat);
        imageCat2 = findViewById(R.id.imageView_cat2);

        //
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.menu_cats, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {

            case R.id.action_cat1_settings:
                Log.i(TAG, "Clicked settings 1- FADING");
                typeOfEvent = TypeOfEvent.FADING;
                return true;

            case R.id.action_cat2_settings:
                Log.i(TAG, "Clicked settings  2 - ROTATE_CW CW");
                typeOfEvent = TypeOfEvent.ROTATE_CW;
                return true;

            case R.id.action_cat_settings_ccw:
                Log.i(TAG, "Clicked settings  2 - ROTATE_CW CCW");
                typeOfEvent = TypeOfEvent.ROTATE_CCW;
                return true;

            default:
                return false;
        }
    }

}
