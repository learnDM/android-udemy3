package com.akordirect.udemy3.domain;

import android.os.AsyncTask;
import android.util.Log;

public class DownloadTask extends AsyncTask<String, Void, String> {
    private static final String TAG = "DownloadTask";

    @Override
    protected String doInBackground(String... strings) {
        Log.d(TAG, "URL - " + strings[0]);

        for(int i = 0; i<200; i++){
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            Log.d(TAG, Integer.toString(i));
        }

        return "DownloadTask (extends AsyncTask) is done!";
    }
}
