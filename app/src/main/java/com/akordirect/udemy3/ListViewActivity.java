package com.akordirect.udemy3;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.SeekBar;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ListViewActivity extends AppCompatActivity {

    private static final String TAG = "ListViewActivity";
    private ListView listView;
    private ListView listView2;
    private SeekBar seekBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_list);

        listView = findViewById(R.id.list_view_dm);
        listView2 = findViewById(R.id.list_view_dm2);
        seekBar = findViewById(R.id.seekBar_listwiev);

        seekBar.setMax(20);
        seekBar.setProgress(10);

        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                int min = 1;
                int timeTablesNumber;
                if (i < min) {
                    timeTablesNumber = min;
                    seekBar.setProgress(min);
                } else {
                    timeTablesNumber = i;
                }
                Log.i(TAG, "timeTablesNumber = " + timeTablesNumber);

                List<String> nombers = new ArrayList<>();
                for(int j = 1; j<=10; j++){
                    nombers.add(Integer.toString(j*timeTablesNumber));
                }

                ArrayAdapter<String> arrayAdapter =
                        new ArrayAdapter<>(ListViewActivity.this, android.R.layout.simple_list_item_1, nombers);

                listView.setAdapter(arrayAdapter);
             }


            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });


        showListOfNames();


    }

    private void showListOfNames(){
//        final List<String> names = Arrays.asList("Hello", "Friend1");
        final List<String> names =  new ArrayList<>();
        names.add("Dmitry");
        names.add("Anna");
        names.add("Oleg");
        names.add("Victoria");
        names.add("Smurf");
        names.add("Jordj");
        names.add("Van");


        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, names);
        listView2.setAdapter(arrayAdapter);
        listView2.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
//                adapterView.setVisibility(View.GONE);
                Log.d("Log", names.get(i) + " is pressed!");
                Toast.makeText(ListViewActivity.this, names.get(i) + " is pressed!", Toast.LENGTH_SHORT).show();
            }
        });
    }

}
