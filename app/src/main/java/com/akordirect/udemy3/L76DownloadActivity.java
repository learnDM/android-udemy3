package com.akordirect.udemy3;

import android.os.Bundle;

import com.akordirect.udemy3.domain.DownloadTask;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.util.Log;
import android.view.View;
import android.widget.Button;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Date;
import java.util.Random;

public class L76DownloadActivity extends AppCompatActivity {
    private static final String TAG = "L76DownloadActivity";
    private Button button ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_l76_download);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        // dm
        button = findViewById(R.id.button_download);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Random r = new Random();
//                button.setText(r.nextInt(100));
                Log.d(TAG, "Button is clicked!!");
            }
        });

        // Work with Async
        String tt = "tt";
        DownloadTask task = new DownloadTask();
        try {
//           tt = task.execute("http://q11.jvmhost.net/").get();
            task.execute("http://q11.jvmhost.net/");
        }catch (Exception e){
            Log.e(TAG, "Exeption = " + e.getMessage());
        }
        Log.d(TAG, "tt = " + tt);
    }

}
